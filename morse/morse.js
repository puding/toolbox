var dictTextToMorse = {
    'A': '.-',    'B': '-...',  'C': '-.-.',  'D': '-..',
    'E': '.',     'F': '..-.',  'G': '--.',   'H': '....',
    'I': '..',    'J': '.---',  'K': '-.-',   'L': '.-..',
    'M': '--',    'N': '-.',    'O': '---',   'P': '.--.',
    'Q': '--.-',  'R': '.-.',   'S': '...',   'T': '-',
    'U': '..-',   'V': '...-',  'W': '.--',   'X': '-..-',
    'Y': '-.--',  'Z': '--..',  ' ': '/',     '\n': '/',
    '1': '.----', '2': '..---', '3': '...--', '4': '....-', 
    '5': '.....', '6': '-....', '7': '--...', '8': '---..', 
    '9': '----.', '0': '-----', 
};

var dictMorseToText = {};
for (let key of Object.keys(dictTextToMorse)) {
    dictMorseToText[dictTextToMorse[key]] = key;
}
dictMorseToText['/'] = ' ';

function translateTextToMorse(text) {
    var toTranslate = text.toUpperCase();
    var translated = [];
    for (let c of toTranslate) {
        if (c in dictTextToMorse) {
            var t = dictTextToMorse[c];
        } else {
            var t = dictTextToMorse['X'];
        }
        translated.push(t);
    }
    return translated.join(' ');
}

function translateMorseToText(morse) {
    var toTranslate = morse.split(/\s+/);
    var translated = [];
    for (let c of toTranslate) {
        if (c in dictMorseToText) {
            var t = dictMorseToText[c];
        } else {
            var t = '?';
        }
        translated.push(t);
    }
    return translated.join('');
}

$('#textToMorseButton').click(function() {
    morseArea.value = translateTextToMorse($('#textArea').val());
});

$('#morseToTextButton').click(function() {
    textArea.value = translateMorseToText($('#morseArea').val());
});

textArea.focus();
