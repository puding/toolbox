function caesar(text, i) {
    translated = [];
    for (c of text) {
        if (c === ' ') {
            translated.push(' ');
        } else {
            translated.push(String.fromCharCode((c.charCodeAt(0)-65+i)%26+65));
        }
    }
    return translated.join('');
}

function atbash(text) {
    translated = [];
    for (c of text) {
        if (c === ' ') {
            translated.push(' ');
        } else {
            translated.push(String.fromCharCode(27-c.charCodeAt(0)+2*64));
        }
    }
    return translated.join('');
}

$('#input').bind('input propertychange', function() {
    var text = $('#input').val().toUpperCase();
    if (/^[A-Z ]*$/.test(text)) {
        $('#input').removeClass('is-invalid');
        for (var i = 0; i < 26; i++) {
            $('#t'+i).html(caesar(text, i));
        }
        $('#atbash').html(atbash(text));
    } else {
        $('#input').addClass('is-invalid');
        for (var i = 0; i < 26; i++) {
            $('#t'+i).html('');
        }
        $('#atbash').html('');;
    }
    
});

$('#input').focus();
