
function getEquations(text) {
    return text.split(',').filter(function(item) {return item !== '';});
}

function solveBatch(permutations, numOfPermutations, stepList, 
    numOfChecked, numOfSols, batch, text, allLetters, mayZero) {
    $('#progressbar').css('width', batch+'%');
    $('#progressbarText').html(batch+'% checked, '+numOfSols+(numOfSols===1 ? ' solution' : ' solutions')+' found');
    // we are done
    if (numOfChecked === numOfPermutations) {
        $('#go').prop('disabled', false);
        $('#input').prop('disabled', false);
        $('#mayZero').prop('disabled', false);
        $('#progressbar').removeClass('progress-bar-striped');
        return;
    }
    // actually solve the batch
    for (let j = stepList[batch]; j < stepList[batch+1]; j++) {
        var digits = permutations.next().value;
        numOfChecked += 1;
        var replaced = text;
        for (let i = 0; i < allLetters.length; i++) {
            replaced = replaced.replace(new RegExp(allLetters[i], 'g'), digits[i]);
        }
        var words = replaced.match(/[0-9]+/g);
        var equations = getEquations(replaced);
        if ((words.every(function(w){return w[0]!=='0';}) || mayZero) && equations.every(eval)) {
            // we've got a solution
            var tr = $('<tr></tr>')
            for (let c of digits) {
                tr.append('<td style="text-align: center;">'+c+'</td>');
            }
            for (let c of equations) {
                tr.append('<td style="text-align: center;">'+c.replace(/===/g, '=')+'</td>');
            }
            $('#tableBody').append(tr);
            numOfSols += 1;
        }
    }
    batch += 1;
    setTimeout(function(){solveBatch(permutations, numOfPermutations, stepList, 
        numOfChecked, numOfSols, batch, text, allLetters, mayZero)}, 200);
}

function solve(text, allLetters, mayZero) {
    var permutations = G.permutation(['0','1','2','3','4','5','6','7','8','9'], allLetters.length);
    var numOfPermutations = G.P(10, allLetters.length);
    var stepList = [];
    for (let i = 0; i < 100; i++) {
        stepList.push(Math.floor(numOfPermutations/100*i));
    }
    stepList.push(numOfPermutations);
    var numOfChecked = 0;
    var numOfSols = 0;
    var batch = 0;
    solveBatch(permutations, numOfPermutations, stepList, 
        numOfChecked, numOfSols, batch, text, allLetters, mayZero);
}

function startSolve() {
    var text = $('#input').val();
    text = text.replace(/\s/g, '');
    text = text.replace(/=/g, '===');
    // reset table nd hide progressbar
    var mayZero = $('#mayZero').prop('checked');
    $('#tableHead').empty();
    $('#tableBody').empty();
    $('#progress').hide();
    $('#progressbar').css('width', '0%');
    $('#progressbarText').html('0% checked, 0 solutions found');
    // get list of used letters and letter equations
    var allLetters = [];
    for (let c of text) {
        if (/^[a-z]$/i.test(c)) {
            allLetters.push(c);
        }
    }
    allLetters = Array.from(new Set(allLetters));
    allLetters.sort();
    var letterEquations = getEquations(text);
    // do nothing if there is nothing to solve or too much letters
    if (letterEquations.length === 0 
            || allLetters.length === 0 
            || allLetters.length > 10 
            || text.indexOf('=')===-1) {
        return;
    }
    // set table header and show progressbar
    $('#go').prop('disabled', true);
    $('#input').prop('disabled', true);
    $('#mayZero').prop('disabled', true);
    $('#progress').show();
    $('#progressbar').addClass('progress-bar-striped');
    for (let c of allLetters) {
        $('#tableHead').append('<th scope="col" style="text-align: center;">'+c+'</th>');
    }
    for (let c of letterEquations) {
        $('#tableHead').append('<th scope="col" style="text-align: center;">'+c.replace(/===/g, '=')+'</th>');
    }
    // lets iterate!
    setTimeout(function(){solve(text, allLetters, mayZero);}, 1500);
}


$('#go').click(function() {
    startSolve();
});

$('#input').focus();
