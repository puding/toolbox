<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Toolbox - Python format strings</title>
    <link rel="icon" type="image/png" href="/toolbox/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/lumen/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="/toolbox/lib/prism.css">
    <link rel="stylesheet" href="/toolbox/main.css">
</head>
<body class="lang-none">
<header id="nav-placeholder"></header>

<main>
<div class="container">

<h1>Python <code>format</code> strings</h1>
<p>
    This is a python <code>format</code> reference. It attempts to be simple yet exhaustive. 
    <a href="https://docs.python.org/3/library/string.html#format-string-syntax">Official documentation</a> 
    is the main source of this document. A nice comparison of old-style and new-style formatting can be found 
    on <a href="https://pyformat.info/">pyformat.info</a>.

</p>


<h2>Replacement field</h2>
<p>
    The content of replacement field <code>{}</code> may consist of three parts - 
    field name, conversion and format specification.
<pre><code>replacement_field ::=  "{" [field_name] ["!" conversion] [":" format_spec] "}"
field_name        ::=  arg_name ("." attribute_name | "[" element_index "]")*
arg_name          ::=  [identifier | digit+]
attribute_name    ::=  identifier
element_index     ::=  digit+ | index_string
index_string      ::=  &lt;any source character except "]"&gt; +
conversion        ::=  "r" | "s" | "a"
format_spec       ::=  &lt;described in the next section&gt;</code></pre>
</p>

<ul>
    <li>
        <em>Field name</em> is either a name or a number (taking appropriate keyword argument 
        or positional argument of <code>format()</code>). An attribute or indexed item can 
        be specified - for example, <code>'{0[2]}'.format([10, 20, 30])</code> returns 
        <code>30</code>, which is third item of first argument passed to <code>format()</code>.
    </li>
    <li>
        <em>Conversion</em> indicates if <code>repr()</code>, <code>str()</code> or 
        <code>ascii()</code> should be called before formatting.
    </li>
    <li>
        Finally, the <em>format specification</em> determines the exact format of returned string (see below).
        <ul>
            <li>
                Note: The format specification field <em>can</em> include nested 
                replacement fields within it. Deeper nesting is not allowed.
            </li>
        </ul>
    </li>
</ul>


<h2>Format specification</h2>
<p>
<pre><code>format_spec     ::=  [[fill]align][sign]["#"]["0"][width][grouping_option][.precision][type]
fill            ::=  <any character>
align           ::=  "&lt;" | "&gt;" | "=" | "^"
sign            ::=  "+" | "-" | " "
width           ::=  digit+
grouping_option ::=  "_" | ","
precision       ::=  digit+
type            ::=  "b" | "c" | "d" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "o" | "s" | "x" | "X" | "%"</code></pre>
</p>


<h3>Fill</h3>
<p>
    Specifies the character which should be used to fill the empty space (so that 
    the string has desired minimum width).
</p>


<h3>Align</h3>
<table class="table table-sm">
    <tr>
        <th style="text-align: center;">Option</th>
        <th>Meaning</th>
    </tr>
    <tr>
        <td style="text-align: center;"><code>&lt;</code></td>
        <td>Forces the field to be left-aligned within the available space (this is the default for most objects).</td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>&gt;</code></td>
        <td>Forces the field to be right-aligned within the available space (this is the default for numbers).</td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>=</code></td>
        <td>
            Forces the padding to be placed after the sign (if any) but before the digits. 
            This is used for printing fields in the form <code>+000000120</code>. This alignment 
            option is only valid for numeric types. It becomes the default when <code>0</code> 
            immediately precedes the field width.
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>^</code></td>
        <td>Forces the field to be centered within the available space.</td>
    </tr>
</table>
<p>
    Note that unless a minimum field width is defined, the field width will always be 
    the same size as the data to fill it, so that the alignment option has no meaning in this case.
</p>


<h3>Sign</h3>
<table class="table table-sm">
    <tr>
        <th style="text-align: center;">Option</th>
        <th>Meaning</th>
    </tr>
    <tr>
        <td style="text-align: center;"><code>+</code></td>
        <td>Indicates that a sign should be used for both positive as well as negative numbers.</td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>-</code></td>
        <td>Indicates that a sign should be used only for negative numbers (this is the default behavior).</td>
    </tr>
    <tr>
        <td style="text-align: center;">space</td>
        <td>Indicates that a leading space should be used on positive numbers, and a minus sign on negative numbers.</td>
    </tr>
</table>


<h3><code>#</code></h3>
<p>
    The <code>#</code> option causes the “alternate form” to be used for the conversion. 
    The alternate form is defined differently for different types. This option is only 
    valid for integer, float, complex and Decimal types. For integers, when binary, octal, 
    or hexadecimal output is used, this option adds the prefix respective 
    <code>0b</code>, <code>0o</code>, or <code>0x</code> to the output value. 
    For floats, complex and Decimal the alternate form causes the result of the 
    conversion to always contain a decimal-point character, even if no digits follow it.
    Normally, a decimal-point character appears in the result of these conversions only 
    if a digit follows it. In addition, for <code>g</code> and <code>G</code> conversions, 
    trailing zeros are not removed from the result.
</p>


<h3><code>0</code></h3>
<p>
    Preceding the width field by a zero character enables sign-aware zero-padding for numeric 
    types. This is equivalent to a fill character of <code>0</code> with an alignment 
    type of <code>=</code>.
</p>


<h3>Width</h3>
<p>
    Decimal integer defining the minimum field width. If not specified, then the 
    field width will be determined by the content.
</p>


<h3>Grouping option</h3>
<table class="table table-sm">
    <tr>
        <th style="text-align: center;">Option</th>
        <th>Meaning</th>
    </tr>
    <tr>
        <td style="text-align: center;"><code>,</code></td>
        <td>
            Signals the use of a comma for a thousands separator. For a locale 
            aware separator, use the <code>n</code> integer presentation type instead.
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"><code>_</code></td>
        <td>
            Signals the use of an underscore for a thousands separator for floating point 
            presentation types and for integer presentation type <code>d</code>. For integer 
            presentation types <code>b</code>, <code>o</code>, <code>x</code>, and <code>X</code>, 
            underscores will be inserted every 4 digits. For other presentation types, 
            specifying this option is an error.
        </td>
    </tr>
</table>


<h3>Precision</h3>
<p>
    A decimal number indicating how many digits should be displayed after the decimal
    point for a floating point value formatted with <code>f</code> and <code>F</code>, 
    or before and after the decimal point for a floating point value formatted with 
    <code>g</code> or <code>G</code>. For non-number types the field indicates the maximum 
    field size - in other words, how many characters will be used from the field content. 
    The precision is not allowed for integer values.
</p>


<h3>Type</h3>
<ul>
    <li>
        The available <em>string</em> presentation types are:
        <table class="table table-sm">
            <tr>
                <th style="text-align: center;">Option</th>
                <th>Meaning</th>
            </tr>
            <tr>
                <td style="text-align: center;"><code>s</code></td>
                <td>String format. This is the default type for strings and may be omitted.</td>
            </tr>
            <tr>
                <td style="text-align: center;">None</td>
                <td>The same as <code>s</code>.</td>
            </tr>
        </table>
    </li>
    <li>
        The available <em>integer</em> presentation types are:
        <table class="table table-sm">
            <tr>
                <th style="text-align: center;">Option</th>
                <th>Meaning</th>
            </tr>
            <tr>
                <td style="text-align: center;"><code>b</code></td>
                <td>Binary format. Outputs the number in base 2.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>c</code></td>
                <td>Character. Converts the integer to the corresponding unicode character before printing.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>d</code></td>
                <td>Decimal Integer. Outputs the number in base 10.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>o</code></td>
                <td>Octal format. Outputs the number in base 8.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>x</code></td>
                <td>Hex format. Outputs the number in base 16, using lower- case letters for the digits above 9.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>X</code></td>
                <td>Hex format. Outputs the number in base 16, using upper- case letters for the digits above 9.</td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>n</code></td>
                <td>
                    Number. This is the same as <code>d</code>, except that it uses the 
                    current locale setting to insert the appropriate number separator characters.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">None</td>
                <td>The same as <code>d</code>.</td>
            </tr>
        </table>
        <p>
            In addition to the above presentation types, integers can be formatted with the floating 
            point presentation types listed below (except 'n' and None). When doing so, float() 
            is used to convert the integer to a floating point number before formatting.
        </p>
    </li>
    <li>
        The available presentation types for <em>floating point and decimal values</em> are:
        <table class="table table-sm">
            <tr>
                <th style="text-align: center;">Option</th>
                <th>Meaning</th>
            </tr>
            <tr>
                <td style="text-align: center;"><code>e</code></td>
                <td>
                    Exponent notation. Prints the number in scientific notation using the 
                    letter <code>e</code> to indicate the exponent. The default precision is 6.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>E</code></td>
                <td>
                    Exponent notation. Same as <code>e</code> except it uses an upper 
                    case <code>E</code> as the separator character.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>f</code></td>
                <td>
                    Fixed-point notation. Displays the number as a fixed-point number. 
                    The default precision is 6.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>F</code></td>
                <td>
                    Fixed-point notation. Same as <code>f</code>, but converts 
                    <code>nan</code> to <code>NAN</code> and <code>inf</code> to <code>INF</code>.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>g</code></td>
                <td>
                    General format. For a given precision <code>p >= 1</code>, this rounds the number 
                    to <code>p</code> significant digits and then formats the result in either fixed-point 
                    format or in scientific notation, depending on its magnitude. The precise rules are 
                    as follows: suppose that the result formatted with presentation type <code>e</code> 
                    and precision <code>p-1</code> would have exponent <code>exp</code>. 
                    Then if <code>-4 <= exp < p</code>, the number is formatted with presentation 
                    type <code>f</code> and precision <code>p-1-exp</code>. Otherwise, the number is 
                    formatted with presentation type <code>e</code> and precision <code>p-1</code>. In 
                    both cases insignificant trailing zeros are removed from the significand, and the decimal 
                    point is also removed if there are no remaining digits following it. Positive and negative 
                    infinity, positive and negative zero, and nans, are formatted as <code>inf</code>, 
                    <code>-inf</code>, <code>0</code>, <code>-0</code> and nan respectively, regardless of the 
                    precision. A precision of <code>0</code> is treated as equivalent to a precision of 
                    <code>1</code>. The default precision is 6.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>G</code></td>
                <td>
                    General format. Same as <code>g</code> except switches to <code>E</code> if the 
                    number gets too large. The representations of infinity and NaN are uppercased, too.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>n</code></td>
                <td>
                    Number. This is the same as <code>g</code>, except that it uses the current 
                    locale setting to insert the appropriate number separator characters.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><code>%</code></td>
                <td>
                    Percentage. Multiplies the number by 100 and displays in 
                    fixed (<code>f</code>) format, followed by a percent sign.
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">None</td>
                <td>
                    Similar to <code>g</code>, except that fixed-point notation, when used, 
                    has at least one digit past the decimal point. The default precision 
                    is as high as needed to represent the particular value. The overall 
                    effect is to match the output of <code>str()</code> as altered by the 
                    other format modifiers.
                </td>
            </tr>
        </table>
    </li>
</ul>


</div>
</main>
<footer id="foot-placeholder"></footer>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="/toolbox/lib/prism.js"></script>
<script src="/toolbox/main.js"></script>
</body>
</html>